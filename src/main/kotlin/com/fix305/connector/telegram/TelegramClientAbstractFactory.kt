package com.fix305.connector.telegram

interface TelegramClientAbstractFactory {
    fun build(): TelegramClient
}