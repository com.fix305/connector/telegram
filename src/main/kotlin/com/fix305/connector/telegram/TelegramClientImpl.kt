package com.fix305.connector.telegram

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json

internal class TelegramClientImpl(
    private val config: TelegramClientConfig,
) : TelegramClient {
    private val client by lazy { HttpClient(CIO) }

    private val json = Json {
        encodeDefaults = true
        ignoreUnknownKeys = true
    }

    override suspend fun send(request: SendMessageRequest): Result<SendMessageResponse> = runCatching {
        val response = withContext(Dispatchers.IO) {
            client.post("${config.baseUrl}/bot${config.token}/sendMessage") {
                parameter("chat_id", request.chatId)
                parameter("text", request.message)
            }
        }

        when (response.status) {
            HttpStatusCode.OK -> {
                val dto = json.decodeFromString<SendMessageResponseDto>(response.bodyAsText())

                SendMessageResponse.Success(
                    messageId = dto.result?.messageId ?: 0
                )
            }
            HttpStatusCode.Forbidden -> {
                val dto = json.decodeFromString<SendMessageResponseDto>(response.bodyAsText())

                SendMessageResponse.Error(
                    errorCode = dto.errorCode ?: 0,
                    description = dto.description ?: ""
                )
            }

            HttpStatusCode.BadRequest -> {
                val dto = json.decodeFromString<SendMessageResponseDto>(response.bodyAsText())

                SendMessageResponse.Error(
                    errorCode = dto.errorCode ?: 0,
                    description = dto.description ?: ""
                )
            }

            else -> {
                // TODO: нужна спецификация, что делать с другими ответами
                error("Invalid response from telegram, status: `${response.status}`")
            }
        }
    }
}
