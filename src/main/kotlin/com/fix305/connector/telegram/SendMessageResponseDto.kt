package com.fix305.connector.telegram

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class SendMessageResponseDto(
    val ok: Boolean,
    @SerialName("error_code")
    val errorCode: Int? = null,
    val description: String? = null,
    val result: SendMessageResponseResultDto? = null
    /**
     * {
     * 	"ok": false,
     * 	"error_code": 403,
     * 	"description": "Forbidden: bot was blocked by the user"
     * }
     *
     * {
     * 	"ok": true,
     * 	"result": {
     * 		"message_id": 55,
     * 		"from": {
     * 			"id": 6356290049,
     * 			"is_bot": true,
     * 			"first_name": "Fix305 sender",
     * 			"username": "Fix305SenderBot"
     * 		},
     * 		"chat": {
     * 			"id": 25754594,
     * 			"first_name": "Anton",
     * 			"last_name": "Fix",
     * 			"username": "Fix305",
     * 			"type": "private"
     * 		},
     * 		"date": 1697286885,
     * 		"text": "Hi!"
     * 	}
     * }
     */
)
