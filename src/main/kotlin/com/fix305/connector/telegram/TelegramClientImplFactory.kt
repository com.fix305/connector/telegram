package com.fix305.connector.telegram

import com.fix305.common.config.Config

class TelegramClientImplFactory(private val config: Config) : TelegramClientAbstractFactory {
    override fun build(): TelegramClient {
        val tgConfig = TelegramClientConfig.create(config)

        return TelegramClientImpl(tgConfig)
    }
}