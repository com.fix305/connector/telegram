package com.fix305.connector.telegram

interface TelegramClient {
    suspend fun send(request: SendMessageRequest): Result<SendMessageResponse>
}
