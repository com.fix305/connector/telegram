package com.fix305.connector.telegram

sealed interface SendMessageResponse {
    data class Success(
        val messageId: Long
    ) : SendMessageResponse

    data class Error(
        val errorCode: Int,
        val description: String
    ) : SendMessageResponse
}