package com.fix305.connector.telegram

data class SendMessageRequest(
    val chatId: Long,
    val message: String
)