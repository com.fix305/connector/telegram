package com.fix305.connector.telegram

import com.fix305.common.config.Config
import com.fix305.common.config.get

internal class TelegramClientConfig private constructor(
    val baseUrl: String = "https://api.telegram.org",
    val token: String
) {
    companion object {
        fun create(config: Config): TelegramClientConfig {
            val serviceConfig = config.getConfig("service")

            return TelegramClientConfig(
                baseUrl = serviceConfig["baseUrlApi"] ?: "https://api.telegram.org",
                token = serviceConfig.getOrThrow("token").getString()
            )
        }
    }
}