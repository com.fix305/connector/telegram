package com.fix305.connector.telegram

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class SendMessageResponseResultDto(
    @SerialName("message_id")
    val messageId: Long,
)
