package com.fix305.connector.telegram

import com.fix305.common.config.ConfigMap
import com.fix305.microservice.ktor.test_fixtures.testMicroservice
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

class TelegramClientImplTest : FunSpec() {
    private val config = TelegramClientConfig.create(ConfigMap(
        "service.baseUrlApi" to "http://127.0.0.1:9305",
        "service.token" to "AAAAAAA:1234567890"
    ))

    private val tgClient: TelegramClient = TelegramClientImpl(config = config)

    init {
        context("Telegram client") {
            test("success") {
                val sendMessage = SendMessageRequest(
                    chatId = 123456,
                    message = "Hello!"
                )

                val createdMessageId = 1559

                var requestedChatId: String? = null
                var requestedMessage: String? = null

                testMicroservice {
                    routing {
                        post("bot${config.token}/sendMessage") {
                            val params = call.parameters

                            requestedChatId = params["chat_id"].toString()
                            requestedMessage = params["text"].toString()

                            call.respond("""
                                {
                                	"ok": true,
                                	"result": {
                                		"message_id": $createdMessageId,
                                		"from": {
                                			"id": 6356290049,
                                			"is_bot": true,
                                			"first_name": "Fix305 sender",
                                			"username": "Fix305SenderBot"
                                		},
                                		"chat": {
                                			"id": 25754594,
                                			"first_name": "Anton",
                                			"last_name": "Fix",
                                			"username": "Fix305",
                                			"type": "private"
                                		},
                                		"date": 1697352994,
                                		"text": "Hi!"
                                	}
                                }
                            """.trimIndent())
                        }
                    }

                }.use {
                    val result = tgClient.send(sendMessage).getOrThrow() as SendMessageResponse.Success

                    result.messageId shouldBe createdMessageId
                }

                requestedChatId shouldBe sendMessage.chatId.toString()
                requestedMessage shouldBe sendMessage.message
            }

            test("forbidden") {
                val sendMessage = SendMessageRequest(
                    chatId = 123456,
                    message = "Hello!"
                )

                testMicroservice {
                    routing {
                        post("bot${config.token}/sendMessage") {
                            call.respond(
                                HttpStatusCode.Forbidden,
                                """
                                    {
                                        "ok": false,
                                        "error_code": 403,
                                        "description": "Forbidden: bot was blocked by the user"
                                    }
                                """.trimIndent()
                            )
                        }
                    }

                }.use {
                    val result = tgClient.send(sendMessage).getOrThrow() as SendMessageResponse.Error

                    result.errorCode shouldBe 403
                    result.description shouldBe "Forbidden: bot was blocked by the user"
                }
            }

            xtest("user not found") {
                1 shouldBe 2
            }
        }
    }
}
