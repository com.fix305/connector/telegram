plugins {
    kotlin("jvm") version "1.9.0"
    kotlin("plugin.serialization") version "1.9.0"
    id("maven-publish")

    java
}

group = "com.fix305.connector"

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/50390469/packages/maven")
    maven("https://gitlab.com/api/v4/projects/50393262/packages/maven")
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.0")

    implementation("com.fix305.common:config:1.0.1")

    implementation("io.ktor:ktor-client-core:2.3.4")
    implementation("io.ktor:ktor-client-cio:2.3.4")
    implementation("io.ktor:ktor-client-cio-jvm:2.3.4")

    testImplementation("io.kotest:kotest-runner-junit5:5.5.5")
    testImplementation(testFixtures("com.fix305.microservice:ktor:1.0.1"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}


publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/51383225/packages/maven")
            name = "GitLab"
            version = findProperty("version") ?: "undefined"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }

    withJavadocJar()
    withSourcesJar()
}